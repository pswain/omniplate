* On testpypi, the packages have different version numbers from PyPi - used by poetry - causing installations to fail.

Two files are uploaded to PyPi.

Follow

https://johnfraney.ca/blog/create-publish-python-package-poetry/



* For swainlab use

poetry publish --build --username swainlab --password idZ2cacKsRgkxDY



* For sphinx

The important files are source/index.rst, which determines the layout of the html, and source/conf.py.
Working versions of these files are stored in forsphinx/.
All directories must be on the path.

Install sphinx-autoapi with pip.

Then

  mkdir docs
  cd docs
  # create directores
  sphinx-quickstart
  # replace conf.py and index.rst
  cp ../forsphinx/* .
  # generate the html
  sphinx-build -b html . _build


[
No longer necessary. Examples of apidoc
# create rst files for omniplate
sphinx-apidoc -f -o source .. ../oldsetup.py ../compare.py
# create rst files for om_code
sphinx-apidoc -f -o source ../om_code
]

* When uploading, run

cd web/software/omniplate
cp -r ~/Dropbox/scripts/ompkg/code/docs/source/_build/html .
chmod -R 755 html

before

put -r *
