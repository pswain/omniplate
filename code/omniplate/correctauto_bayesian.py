"""A Bayesian version of Lichten ... Swain, BMC Biophys 2014."""

import numpy as np
from scipy.optimize import minimize
from tqdm import tqdm
from dataclasses import dataclass

import omniplate.corrections as omcorr
import omniplate.omgenutils as gu
import omniplate.sunder as sunder

rng = np.random.default_rng()

# notation follows Lichten ... Swain 2014
# GFP is denoted y; AutoFL is denoted z.
# The reference strain is denoted WT.


@dataclass
class om_data:
    """Variables required for posterior."""

    y: np.ndarray
    ywt: np.ndarray
    z: np.ndarray
    zwt: np.ndarray
    dy: np.ndarray
    dywt: np.ndarray
    dz: np.ndarray
    dzwt: np.ndarray
    lyn: np.ndarray
    lzn: np.ndarray
    ly: np.ndarray
    lywt: np.ndarray
    lz: np.ndarray
    lzwt: np.ndarray
    a1y: float = None
    a1z: float = None
    Sy: float = None
    Sz: float = None


def discard_isolated(y, max_gap, replace_value=0):
    """
    Replace entries bracketed by too many zeros.

    Less perturbative than a median filter.
    """
    # find the number of zeros between non-zero entries
    non_zero_indices = np.nonzero(y)[0]
    if non_zero_indices.size == 0:
        return y
    zeros_start = non_zero_indices[0]
    zeros_between = np.diff(non_zero_indices) - 1
    zeros_end = y.size - non_zero_indices[-1] - 1
    no_zeros_between = np.concatenate(
        ([zeros_start], zeros_between, [zeros_end])
    )
    # find sufficiently large contigs of zeros
    sufficient_gaps = no_zeros_between > max_gap
    # create dict of results for each index of a positive entry
    zeros_either_side = {}
    for i, nzi in enumerate(non_zero_indices):
        discard = False
        if nzi == 0:
            if sufficient_gaps[i]:
                discard = True
            zeros_either_side[nzi] = [
                [None, no_zeros_between[i]],
                discard,
            ]
        elif nzi == y.size - 1:
            if sufficient_gaps[i]:
                discard = True
            zeros_either_side[nzi] = [
                [no_zeros_between[i], None],
                discard,
            ]
        else:
            if sufficient_gaps[i] and sufficient_gaps[i + 1]:
                discard = True
            zeros_either_side[nzi] = [
                [
                    no_zeros_between[i],
                    no_zeros_between[i + 1],
                ],
                discard,
            ]
    # find points with large contigs either side
    discards = [nzi for nzi in zeros_either_side if zeros_either_side[nzi][1]]
    # discard
    if np.any(discards):
        y[discards] = replace_value
    return y


def de_nan(y, z, most=True):
    """Remove NaN by discarding some replicates."""
    # NaNs are generated because experiments have different durations
    allnancols = np.all(np.isnan(y), axis=0)
    y = y[:, ~allnancols]
    z = z[:, ~allnancols]
    counts = np.array(
        [np.where(~np.isnan(y[i, :]))[0].size for i in range(y.shape[0])]
    )
    if most:
        # choose the most replicates per time point
        keep = counts == counts.max()
    else:
        # choose the replicates with the longest duration
        keep = counts == counts[np.nonzero(counts)[0][-1]]
    return y[keep, :], z[keep, :], keep


def update_stats_dict(stats_dict, y, z, ywt, zwt, yn, zn):
    """Update stats_dict for current time point."""
    stats_dict["gmax"] = np.min(y) - np.min(ywt)
    stats_dict["amax"] = np.min(ywt)
    stats_dict["y"] = y.flatten()
    stats_dict["z"] = z.flatten()
    stats_dict["ywt"] = ywt.flatten()
    stats_dict["zwt"] = zwt.flatten()
    stats_dict["lyn"] = np.log(yn.flatten())
    stats_dict["lzn"] = np.log(zn.flatten())
    stats_dict["no_y"] = y.size + ywt.size + yn.size
    stats_dict["no_z"] = z.size + zwt.size + zn.size


def define_om_data(theta, stats_dict):
    """Define an om_data instance to calculate the posterior."""
    g, a, ra = theta
    no_y = stats_dict["no_y"]
    no_z = stats_dict["no_z"]
    p = om_data(
        y=stats_dict["y"],
        ywt=stats_dict["ywt"],
        z=stats_dict["z"],
        zwt=stats_dict["zwt"],
        dy=1 / (stats_dict["y"] - a - g),
        dywt=1 / (stats_dict["ywt"] - a),
        dz=1 / (stats_dict["z"] - ra * a - stats_dict["rg"] * g),
        dzwt=1 / (stats_dict["zwt"] - ra * a),
        lyn=stats_dict["lyn"],
        lzn=stats_dict["lzn"],
        ly=np.log(stats_dict["y"] - a - g),
        lywt=np.log(stats_dict["ywt"] - a),
        lz=np.log(stats_dict["z"] - ra * a - stats_dict["rg"] * g),
        lzwt=np.log(stats_dict["zwt"] - ra * a),
    )
    p.a1y = (np.sum(p.ly) + np.sum(p.lywt) + np.sum(p.lyn)) / no_y
    p.a1z = (np.sum(p.lz) + np.sum(p.lzwt) + np.sum(p.lzn)) / no_z
    p.Sy = (
        np.sum((p.ly - p.a1y) ** 2)
        + np.sum((p.lywt - p.a1y) ** 2)
        + np.sum((p.lyn - p.a1y) ** 2)
    )
    p.Sz = (
        np.sum((p.lz - p.a1z) ** 2)
        + np.sum((p.lzwt - p.a1z) ** 2)
        + np.sum((p.lzn - p.a1z) ** 2)
    )
    return p


def minus_log_prob(theta, stats_dict):
    """Get minus log posterior."""
    p = define_om_data(theta, stats_dict)
    # negative log posterior
    mlp = (
        (stats_dict["no_y"] / 2 - 1) * np.log(p.Sy)
        + np.sum(p.ly)
        + np.sum(p.lywt)
        + (stats_dict["no_z"] / 2 - 1) * np.log(p.Sz)
        + np.sum(p.lz)
        + np.sum(p.lzwt)
    )
    return mlp


def jac(theta, stats_dict):
    """Get Jacobian of minus log posterior."""
    p = define_om_data(theta, stats_dict)
    g, a, ra = theta
    no_y, no_z, rg = stats_dict["no_y"], stats_dict["no_z"], stats_dict["rg"]
    # jac for a1
    jac_a1y = (
        np.array(
            [
                -np.sum(p.dy),
                -np.sum(p.dy) - np.sum(p.dywt),
                0,
            ]
        )
        / no_y
    )
    jac_a1z = (
        np.array(
            [
                -rg * np.sum(p.dz),
                -ra * np.sum(p.dz) - ra * np.sum(p.dzwt),
                -a * np.sum(p.dz) - a * np.sum(p.dzwt),
            ]
        )
        / no_z
    )
    # jac for posterior
    jac = np.zeros(3)
    # with respect to g
    dy = (
        -np.sum(2 * (p.ly - p.a1y) * (p.dy + jac_a1y[0]))
        - np.sum(2 * (p.lyn - p.a1y) * jac_a1y[0])
        - np.sum(2 * (p.lywt - p.a1y) * jac_a1y[0])
    ) / p.Sy - np.sum(p.dy)
    dz = (
        -np.sum(2 * (p.lz - p.a1z) * (rg * p.dz + jac_a1z[0]))
        - np.sum(2 * (p.lzn - p.a1z) * jac_a1z[0])
        - np.sum(2 * (p.lzwt - p.a1z) * jac_a1z[0])
    ) / p.Sz - rg * np.sum(p.dz)
    jac[0] = dy + dz
    # with respect to a
    dy = (
        (
            -np.sum(2 * (p.ly - p.a1y) * (p.dy + jac_a1y[1]))
            - np.sum(2 * (p.lywt - p.a1y) * (p.dywt + jac_a1y[1]))
            - np.sum(2 * (p.lyn - p.a1y) * jac_a1y[1])
        )
        / p.Sy
        - np.sum(p.dy)
        - np.sum(p.dywt)
    )
    dz = (
        (
            -np.sum(2 * (p.lz - p.a1z) * (ra * p.dz + jac_a1z[1]))
            - np.sum(2 * (p.lzwt - p.a1z) * (ra * p.dzwt + jac_a1z[1]))
            - np.sum(2 * (p.lzn - p.a1z) * jac_a1z[1])
        )
        / p.Sz
        - ra * np.sum(p.dz)
        - ra * np.sum(p.dzwt)
    )
    jac[1] = dy + dz
    # with respect to ra
    jac[2] = (
        (
            -np.sum(2 * (p.lz - p.a1z) * (a * p.dz + jac_a1z[2]))
            - np.sum(2 * (p.lzwt - p.a1z) * (a * p.dzwt + jac_a1z[2]))
            - np.sum(2 * (p.lzn - p.a1z) * jac_a1z[2])
        )
        / p.Sz
        - a * np.sum(p.dz)
        - a * np.sum(p.dzwt)
    )
    return jac


def set_up_minimization(stats_dict):
    """Set up bounds and initial guess."""
    g_bounds = (0, stats_dict["gmax"])
    a_bounds = (0, stats_dict["amax"])
    ra_bounds = (stats_dict["ramin"], stats_dict["ramax"])
    bounds = [g_bounds, a_bounds, ra_bounds]
    dyywt = stats_dict["y"] - stats_dict["ywt"]
    x0_o = np.array(
        [
            np.median(dyywt[dyywt > 0]) * 1.02,
            np.median(stats_dict["ywt"]) * 0.98,
            np.median(stats_dict["zwt"] / stats_dict["ywt"]) * 0.99,
        ]
    )
    if stats_dict["x0"] is None:
        x0 = x0_o
    else:
        x0 = stats_dict["x0"]
    return bounds, x0, x0_o


def find_mode(stats_dict, no_attempts, time):
    """Find most probable value of g, a, and ra."""
    # Tried with a sequential approach, using a Gaussian
    # approximation of the current posterior as the prior
    # for the future one, but performs worse, perhaps because
    # of the wide hessian.
    bounds, x0, x0_o = set_up_minimization(stats_dict)
    min_mlp = np.inf
    mode = None
    # rands = rng.standard_normal((no_attempts, 3))
    for i in range(no_attempts):
        # if i < int(no_attempts / 2):
        #     # start from proceeding optimum
        #     sampled_x0 = rands[i, :] * 10 * np.sqrt(x0) + x0
        # else:
        #     # start from a new guess
        #     sampled_x0 = rands[i, :] * 10 * np.sqrt(x0_o) + x0_o
        # sampled_x0[sampled_x0 < 0] = 0.01
        # pick a
        a = rng.random() * np.min(stats_dict["ywt"])
        g = np.max([0.01, rng.random() * (np.min(stats_dict["y"]) - a)])
        ra = np.max(
            [
                0.01,
                (
                    rng.random()
                    * np.min(
                        np.hstack(
                            (
                                stats_dict["z"] - stats_dict["rg"] * g,
                                stats_dict["zwt"],
                            )
                        )
                    )
                    / a
                ),
            ]
        )
        cons = [
            {
                "type": "ineq",
                "fun": lambda x: x[1] - np.min(stats_dict["ywt"]),
            },
            {
                "type": "ineq",
                "fun": lambda x: x[0] + x[1] - np.min(stats_dict["y"]),
            },
            {
                "type": "ineq",
                "fun": lambda x: x[2] * x[1]
                + stats_dict["rg"] * x[0]
                - np.min(stats_dict["z"]),
            },
            {
                "type": "ineq",
                "fun": lambda x: x[2] * x[1] - np.min(stats_dict["zwt"]),
            },
        ]
        sampled_x0 = [g, a, ra]
        res = minimize(
            lambda x: minus_log_prob(x, stats_dict),
            x0=sampled_x0,
            bounds=bounds,
            constraints=cons,
            # jac=lambda x: jac(x, stats_dict),
            # method="L-BFGS-B",
            method="COBYLA",
        )
        if res.success:
            if res.fun < min_mlp:
                mode = res.x
                min_mlp = res.fun
                stats_dict["x0"] = res.x
    if mode is None:
        print(
            " Warning: Maximising posterior probability failed at "
            f"time {time:.2f}."
        )
        breakpoint()
        mode = np.nan * np.ones(3)
        stats_dict["x0"] = None
    return mode


def sufficient_replicates(ywt, yn, refstrain, c):
    """Check sufficient numbers of replicates."""
    sufficient = True
    if ywt.shape[1] < 3:
        print(
            f"There are only {ywt.shape[1]} replicates (<3)"
            f" for the {refstrain} strain in {c}."
        )
        print("Abandoning correctauto.")
        sufficient = False
    if yn.shape[1] < 3:
        print(
            f"There are only {yn.shape[1]} replicates (<3)"
            f" for the Null strain in {c}."
        )
        print("Abandoning correctauto.")
        sufficient = False
    return sufficient


def get_tagged_strain_data(self, e, c, s, f, od_keep):
    """Get and check data for fluorescently tagged strain."""
    success = True
    # get data for tagged strain
    _, (y, z, od) = sunder.extractwells(
        self.r, self.s, e, c, s, f.copy() + ["OD"]
    )
    y, z, _ = de_nan(y, z)
    # make OD match t
    od = od[np.nonzero(od_keep)[0], :]
    if y.size == 0 or z.size == 0 or od.size == 0:
        print(f"Warning: No data found for {e}: {s} in {c}!!")
        success = False
    if y.shape[1] < 3:
        print(
            f"There are only {y.shape[1]} replicates (<3)"
            f" for {e}: {s} in {c}."
        )
        print("Abandoning correctauto.")
        success = False
    return y, z, od, success


def correctauto_bayesian(
    self,
    f,
    refstrain,
    experiments,
    experimentincludes,
    experimentexcludes,
    conditions,
    conditionincludes,
    conditionexcludes,
    strains,
    strainincludes,
    strainexcludes,
    **kwargs,
):
    """
    Correct fluorescence for auto- and background fluorescence.

    Use a Bayesian method to correct for autofluorescence from fluorescence
    measurements at two wavelengths and for background fluorescence.

    Implement demixing following Lichten ... Swain, BMC Biophys 2014.
    Invert
        y = g + a
        z = g * r_g + a * r_a
        ywt = a
        zwt = a * r_a
    where we assume that awt = a.
    """
    if len(f) != 2:
        raise Exception(
            "correctauto: the bayesian method needs two "
            "fluorescence measurements."
        )
    print("Using Bayesian approach for two fluorescence wavelengths.")
    print(f"Correcting autofluorescence using {f[0]} and {f[1]}.")
    bname = "bc" + f[0]
    bd_default = {0: (-2, 8), 1: (-2, 8), 2: (-2, 0)}
    if kwargs["bd"] is not None:
        bdn = gu.mergedicts(original=bd_default, update=kwargs["bd"])
    else:
        bdn = bd_default
    for e in sunder.getset(
        self,
        experiments,
        experimentincludes,
        experimentexcludes,
        labeltype="experiment",
        nonull=True,
    ):
        for c in sunder.getset(
            self,
            conditions,
            conditionincludes,
            conditionexcludes,
            labeltype="condition",
            nonull=True,
            nomedia=True,
        ):
            # get data for reference strain
            # y for emission at 525; z for emission at 585
            t, (ywt, zwt) = sunder.extractwells(
                self.r, self.s, e, c, refstrain, f
            )
            ywt, zwt, od_keep = de_nan(ywt, zwt)
            t = t[od_keep]
            # get data for Null
            _, (yn, zn) = sunder.extractwells(self.r, self.s, e, c, "Null", f)
            yn, zn, _ = de_nan(yn, zn)
            if not sufficient_replicates(ywt, yn, refstrain, c):
                continue
            for s in sunder.getset(
                self,
                strains,
                strainincludes,
                strainexcludes,
                labeltype="strain",
                nonull=True,
            ):
                if (
                    s != refstrain
                    and f"{s} in {c}" in self.allstrainsconditions[e]
                ):
                    # get data for tagged strain
                    y, z, od, success = get_tagged_strain_data(
                        self, e, c, s, f, od_keep
                    )
                    if not success:
                        continue
                    print(f"{e}: {s} in {c}")
                    # check if predicted_fl already estimated
                    df = self.s.query(
                        "experiment == @e and condition == @c "
                        "and strain == @s"
                    )
                    if f"inferred_{f[0]}" in df.columns:
                        potential_inferred_fl = (
                            df[f"inferred_{f[0]}"].dropna().values
                        )
                    # use exisiting inferred data or perform inference
                    if not kwargs["restart"] and (
                        "potential_inferred_fl" in locals()
                        and potential_inferred_fl.size == t.size
                    ):
                        print("Using existing inferred fluorescence.")
                        inferred_fl = potential_inferred_fl
                    else:
                        # correct autofluorescence for each time point
                        inferred_fl = np.zeros(t.size)
                        stats_dict = {
                            "x0": None,
                            "rg": self._gamma,
                            "ramin": 0,
                            "ramax": 1,
                        }
                        for i in tqdm(range(t.size)):
                            update_stats_dict(
                                stats_dict,
                                y[i, :],
                                z[i, :],
                                ywt[i, :],
                                zwt[i, :],
                                yn[i, :],
                                zn[i, :],
                            )
                            posterior_mode = find_mode(
                                stats_dict,
                                kwargs["no_minimisation_attempts"],
                                time=t[i],
                            )
                            inferred_fl[i] = posterior_mode[0]
                    # zero inferred fluorescence values with few neighbours
                    to_smooth_fl = discard_isolated(
                        inferred_fl.copy(), kwargs["max_gap"]
                    )
                    # smooth with GP and add to data frames
                    # better errors than smoothing samples of flperod
                    print("Smoothing...")
                    flperod = omcorr.sample_flperod_with_GPs(
                        self,
                        f"inferred_{f[0]}",
                        t,
                        to_smooth_fl,
                        od,
                        kwargs["flcvfn"],
                        bdn,
                        kwargs["nosamples"],
                        e,
                        c,
                        s,
                        max_data_pts=None,  # predicted_fl is 1D
                        figs=kwargs["figs"],
                        logs=True,
                        negs2nan=True,
                    )
                    # store results for fluorescence per OD
                    store_results(
                        self,
                        f,
                        e,
                        c,
                        s,
                        t,
                        inferred_fl,
                        flperod,
                        bname,
                        kwargs["max_err_to_mean"],
                    )
                    print("---")


def store_results(
    self, f, e, c, s, t, inferred_fl, flperod, bname, max_err_to_mean
):
    """Create dict of results and add to data frames."""
    # store results for fluorescence per OD
    autofdict = {
        "experiment": e,
        "condition": c,
        "strain": s,
        "time": t,
        f"inferred_{f[0]}": inferred_fl,
    }
    autofdict[f"{bname}perOD"] = np.mean(flperod, 1)
    autofdict[f"{bname}perOD_err"] = np.std(flperod, 1)
    # low-error predictions
    err_keep = np.where(
        autofdict[f"{bname}perOD_err"]
        < autofdict[f"{bname}perOD"] * max_err_to_mean
    )
    # sections where predicted_fl are zero go to nan
    changes = np.diff(np.concatenate(([0], inferred_fl == 0, [0])))
    start_i = np.where(changes == 1)[0]
    end_i = np.where(changes == -1)[0]
    contigs = np.where(end_i - start_i > 1)[0]
    # define new strict keys: low error and contiguous
    for key in [f"{bname}perOD", f"{bname}perOD_err"]:
        autofdict[f"s_{key}"] = np.ones(autofdict[key].shape) * np.nan
        autofdict[f"s_{key}"][err_keep] = autofdict[key][err_keep]
        for contig in contigs:
            autofdict[f"s_{key}"][start_i[contig] : end_i[contig]] = np.nan
    # add to data frames
    omcorr.addtodataframes(self, autofdict)
