# !/usr/bin/env python

import matplotlib.pyplot as plt
import seaborn as sns

from omniplate.platereader import platereader

__version__ = "1.14"

plt.rcParams["figure.max_open_warning"] = 0
sns.set()
